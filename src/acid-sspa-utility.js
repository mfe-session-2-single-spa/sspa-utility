// Anything exported from this file is importable by other in-browser modules.
import { BehaviorSubject } from "rxjs";

export const cartInfo$ = new BehaviorSubject({
  items: [],
  user: '',
});

export function addItem(itemName, user){
  const items = [ itemName, ...cartInfo$.value.items];
  cartInfo$.next({
    items, user
  });
}

//Esta logica necesita mejoras ;)
export function removeItem(){
  const size = cartInfo$.value.items.length;
  const user = cartInfo$.value.user;
  const items = [ ...cartInfo$.value.items.slice(0, size - 1)];
  cartInfo$.next({
    items, user
  });
}
